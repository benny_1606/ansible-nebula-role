# Fork of "Ansible role for Nebula"

## Origin

Fork from: https://github.com/Tobishua/ansible-nebula

Author stated in LICENSE: Mark Harrison
GitHub: https://github.com/Tobishua

## Changes

* Support Ansible 3
* Drop support for `nebula_unsafe_routes`
* Update hard coded nebula binary url; Add checksum;
* Fix `static_host_map`, `lighthouse.hosts`
* Use `ansible.netcommon.ipaddr` to find external ip addr.
  * Requires `ipaddr` installed on the control node
  * Host fails to detect ext. ip (suggests behind NAT), will not add to `static_host_map`, lighthouse maybe needed
* Add var `nebula_cipher` to customize cipher, default to `chachapoly` instead of `aes`
* Restart nebula systemd service after installation
* Template respects `nebula_cert_path` instead of hard coded path
* Add documentations of lighthouses configuration in README.md

---

# Ansible role for Nebula

[Nebula](https://github.com/slackhq/nebula/) is a scalable overlay networking tool with a focus on performance, simplicity and security.

And this ansible role for deploy nebula regular node and lighthouse node.

For this role you should have preconfigured host with nebula-cert which available via ansible.

## Examples

Dir `example` includes:

* `inventory.yaml`
* `nebula.yaml` - playbook of using this role, and host_vars

## Configure & Install

 1. Place role directory in your Ansible installation
 2. Create playbook and change host_vars for each hosts
 3. Install `ipaddr` on the control node (See [Ansible docs](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters_ipaddr.html))

For all nodes, place hosts in `nebula` group in the inventory

For lighthouses,

* Place hosts in **both** `nebula` and `nebula_lighthouse` groups in the inventory
* Set host_vars `nebula_lighthouse` to `true` in `nebula` group
* Set host_vars `external_addr` in the `nebula_lighthouse` group

### host_vars variables

<details>
<summary>Click this to collapse/fold the list of host_vars.</summary>

`nebula_ip` - Internal IP (inside nebula network) for this host

`nebula_host` - IP address to bind nebula to (default: 0.0.0.0)

`nebula_port` - Nebula port (default: 4242)

`nebula_subnet` - Subnet size for sign in certificate (default: 8)

`nebula_groups` - Groups for host (default: nebula,node)

`nebula_log_level` - Log level (panic, fatal, error, warning, info, or debug) (default: info)

`nebula_log_format` - Log format, text or json (default: text)

`nebula_dev_tun` - Name of the tun device (default: nebula 1)

`nebula_mtu` - MTU for nebula interface (default: 1300)

`nebula_tx_queue` - Transmit queue length

`nebula_lighthouse` - Used to enable lighthouse functionality for a node (default: false)

`nebula_dns` - DNS listener (default: false)

`nebula_dns_port` - Port for DNS listener (default: 53)

`nebula_dns_host` - The DNS host defines the IP to bind the dns listener to (default: 0.0.0.0)

`nebula_update_interval` - Number of seconds between updates from this node to a lighthouse (default: 60)

`nebula_prometheus_host` - IP to bind prometheus to (default: 127.0.0.1)

`nebula_prometheus_port` - Port for prometheus (default: 9091)

`nebula_prometheus_path` - Metrics path (default: metrics)

`nebula_drop_multicast` - Toggles forwarding of multicast packets (default: false)

`nebula_drop_local_broadcast` - Toggles forwarding of local broadcast packets the address of which depends on the ip/mask encoded in pki.cert (default: false)

`nebula_cipher` - Cipher of nebula, "chachapoly" (default) or "aes"

`nebula_inbound_rules` - Rules for inbound traffic

`nebula_outbound_rules` - Rules for outbound traffic

</details>


# License

  Copyright (C) 2013 Mark Harrison
  Copyright (C) 2021 Benny S

[MIT License](https://github.com/tobishua/ansible-nebula/blob/master/LICENCE).
